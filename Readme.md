# Caddy-with-Cloudflare

This repo is a simple Dockerfile and Gitlab-Pipeline to take the caddy builder image and add the cloudflare "extension" to let let's encrypt handle the DNS Challenges via Cloudflare.